# GitLab Figma Plugin
This [Figma plugin](https://www.figma.com/plugin-docs/intro/) lets users to upload images to GitLab Design Tabs from Figma. Based on the idea of the [GitLab Sketch Plugin](https://gitlab.com/gitlab-com/gitlab-ux/gitlab-sketch-plugin). The issue that kickstarted this project can be found [here](https://gitlab.com/gitlab-org/gitlab/-/issues/207433#note_294309127).

## User Flow
![image](/uploads/e183d089f4868c108616e79644622426/image.png)

1. Select Frame to upload.
2. Open Plugin from the Figma UI.
3. Review if the selected Frame is correct.
4. Choose Issue or To-do where the image should be uploaded to.
5. Upload selection.
6. Close the modal.

## Layout
The layout should be based on the [Modal](https://design.gitlab.com/components/modals) component.
