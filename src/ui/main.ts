import Vue from 'vue';
// @ts-ignore
import App from './app.vue';
// @ts-ignore
import router from './router.ts';
// @ts-ignore
import store from './store.ts';

import '@gitlab/ui/src/scss/gitlab_ui.scss';

Vue.config.productionTip = false;

new Vue({
  router,
  store,
  render: h => h(App)
}).$mount('#app');
