import Vue from 'vue';
import Router from 'vue-router';
import Selection from './views/selection.vue';
import Destination from './views/destination.vue';
import Success from './views/success.vue';

Vue.use(Router);

export default new Router({
  mode: 'hash',
  base: process.env.BASE_URL,
  routes: [
    {
      path: '',
      name: 'selection',
      component: Selection
    },
    {
      path: '/destination',
      name: 'destination',
      component: Destination
    },
    {
      path: '/success',
      name: 'success',
      component: Success
    }
  ]
});
